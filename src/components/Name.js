import React from 'react';
import { connect } from 'react-redux';
import { setName } from '../actions/nameActions';

class Name extends React.Component {

  handleNameChange(e) {
    let value = e.target.value;
    this.props.setName(value);
  }

  render() {
    return (
      <section>
        <input onChange={this.handleNameChange.bind(this)} type="text"/>
        <p>hello, {this.props.name}</p>
      </section>
    )
  }
}

// const mapDispatchToProps = dispatch => bindActionCreators({
//   setName
// }, dispatch);
// const mapStateToProps = state => state.nameReducer;

export default connect(state => state.nameReducer, { setName })(Name);

